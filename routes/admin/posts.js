const express = require('express');
const router = express.Router();
const Post = require('../../models/Post');
const Category = require('../../models/Category');
const { isEmpty, uploadDir } = require('../../helpers/upload-helper');
const fs = require('fs');
// const { userAuthenticated } = require('../../helpers/authentication');

// Overrite default route because of admin and home 
router.all('/*', (req, res, next) =>{

  req.app.locals.layout = 'admin';
  next();

});

// Get Data out of the Database and render in All Posts page
router.get('/', (req, res) => {
  
  Post.find({})

    .populate('category')
    
    .then(posts => {    

      res.render('admin/posts', {posts: posts});
  
  })

});

// Route for only loged user visible posts
router.get('/my-posts', (req, res) => {

  Post.find({user: req.user.id})
  
    .populate('category')
    
    .then(posts => {

    res.render('admin/posts/my-posts', {posts: posts});
  
  })


});


// Render Create Post page
router.get('/create', (req, res) => {

  Category.find({}).then(categories =>{

    res.render('admin/posts/create', {categories: categories});

  });

});

// Post Created Post Data to database
router.post('/create', (req, res) => {

  let errors = [];

  if(!req.body.title) {

    errors.push({message: 'Please add a title'});

  }

  if(!req.body.body) {

    errors.push({message: 'Please add a description'});

  }

  if(errors.length > 0) {
    
    res.render('admin/posts/create', { errors: errors});

  } else {

  let filename = 'ispanu-kalbos-kursai-dovanu kuponas.png';

  if (!isEmpty(req.files)) {

    let file = req.files.file;
    filename = Date.now() + '-' + file.name;
  
    file.mv('./public/uploads/' + filename, (err) => {
      
      if(err) throw err;
  
    });

  }

  console.log(req.files);

  let allowComments = true;

  if(req.body.allowComments){
      allowComments = true;
  } else {
    allowComments = false;
  }

  const newPost = new Post({

    user: req.user.id,
    title: req.body.title,
    status: req.body.status,
    allowComments: allowComments,
    body: req.body.body,
    category: req.body.category,
    file: filename

  });

  newPost.save().then(savedPost => {

    req.flash('success_message', `Post ${savedPost.title} was created successfully`);

    res.redirect('/admin/posts');

  }).catch(error => {

    console.log(error, 'Could not save data');

  });

  console.log(req.body);

}
});

// Render Edit Post page
router.get('/edit:id', (req, res) => {

  Post.findOne({_id: req.params.id})
  .then(post => {

    
    Category.find({}).then(categories =>{

      res.render('admin/posts/edit', {post: post, categories: categories});

    });

  });

  // res.render('admin/posts/edit');

});

// Put request for updating database with edited values
router.put('/edit:id', (req, res) => {

  Post.findOne({_id: req.params.id}).then(post => {
    
    let allowComments = true;

    if(req.body.allowComments){
        allowComments = true;
    } else {
      allowComments = false;
    }

    post.user = req.user.id;
    post.title = req.body.title;
    post.status = req.body.status;
    post.allowComments = allowComments;
    post.body = req.body.body;
    post.category = req.body.category;

    if (!isEmpty(req.files)) {

      let file = req.files.file;
      filename = Date.now() + '-' + file.name;
      post.file = filename;
    
      file.mv('./public/uploads/' + filename, (err) => {
        
        if(err) throw err;
    
      });
  
    }

    post.save().then(updatedPost => {

      req.flash('success_message', 'Post was successfully updated');

      res.redirect('/admin/posts/my-posts');

    });

  });

});

// Delete Route
router.delete('/:id', (req, res) => {

  Post.findOne({_id: req.params.id})
  
  .populate('comments')
  .then(post => {

    fs.unlink(uploadDir + post.file, (err) => {
      
      if (!post.comments.length < 1) {

        post.comments.forEach(comment => {

          comment.remove();

        });

      }

      post.remove().then(postRemoved => {
        
        req.flash('success_message', 'Post was successfully deleted');
        res.redirect('/admin/posts/my-posts');
        
      });

    });

  });

});



module.exports = router;