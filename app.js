const express = require('express');
const app = express();
const path = require('path');
const exphbs = require('express-handlebars');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const upload = require('express-fileupload');
const session = require('express-session');
const flash = require('connect-flash');
const { mongoDbUrl } = require('./config/database');
const passport = require('passport');
 

mongoose.connect(mongoDbUrl, { useNewUrlParser: true }).then(db => {
  console.log('MONGO connected')
}).catch(err => console.log(err));

const port = process.env.PORT || 4500;

app.listen(port, () => {
  
  console.log(`listening on port ${port}`);

});

app.use(express.static(path.join(__dirname, 'public')));

// accesing hepler functions
const {select, generateDate, paginate} = require('./helpers/handlebars-helpers');

//Set View Engine
app.engine('handlebars', exphbs({defaultLayout: 'home', helpers: {select: select, generateDate: generateDate, paginate: paginate}}));
app.set('view engine', 'handlebars');

// Upload Middleware
app.use(upload());


// Body Parser
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// Method Override
app.use(methodOverride('_method'));


// Sessions
app.use(session({

    secret: 'evelina123lovecoding',
    resave: true,
    saveUninitialized: true

}));

// Midleware for flash
app.use(flash());

// PASSPORT
app.use(passport.initialize());
app.use(passport.session());

// Local variables using Middleware
app.use((req, res, next) => {

  res.locals.user = req.user || null;

  res.locals.success_message = req.flash('success_message');

  res.locals.error_message = req.flash('error_message');

  res.locals.error = req.flash('error');

  next();

});

// Load Routes
const mainRoute = require('./routes/home/index');
const mainRouteAdmin = require('./routes/admin/index');
const posts = require('./routes/admin/posts');
const categories = require('./routes/admin/categories');
const comments = require('./routes/admin/comments');
// User Routes
app.use('/', mainRoute);
app.use('/admin', mainRouteAdmin);
app.use('/admin/posts', posts);
app.use('/admin/categories', categories);
app.use('/admin/comments', comments);

